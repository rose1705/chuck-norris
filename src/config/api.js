const BASE_URL = `http://api.icndb.com`;

export default {
    tenJokes: () => `${BASE_URL}/jokes/random/10`,
    oneJoke: () => `${BASE_URL}/jokes/random/1`,
};
