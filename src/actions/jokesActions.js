import api from "../config/api";
import constants from "../config/constants";

//TODO: improve error handling
//TODO: remove constants (boilerplate)

export const fetchTenJokes = () => async dispatch => {
    dispatch({ type: constants.FETCHING_JOKES });
    try {
        const response = await fetch(api.tenJokes());

        if (!response.ok) {
            throw new Error("Failed to fetch your jokes...");
        }
        const data = await response.json();
        response.ok && dispatch({ type: constants.JOKES_RECEIVED, data: data });
    } catch (e) {
        console.log("Something went wrong", e);
    }
};

export const addToFavorites = joke => dispatch => {
    dispatch({ type: constants.ADD_FAVORITE, data: joke });
};

export const removeFromFavorites = joke => dispatch => {
    dispatch({ type: constants.REMOVE_FAVORITE, data: joke });
};

export const addRandomToFavorite = () => async dispatch => {
    dispatch({ type: constants.FETCHING_JOKE });
    try {
        const response = await fetch(api.oneJoke());

        if (!response.ok) {
            throw new Error("Failed to fetch a random joke...");
        }
        const data = await response.json();
        response.ok &&
            dispatch({ type: constants.ADD_FAVORITE, data: data.value[0] });
    } catch (e) {
        console.log("Something went wrong", e);
    }
};
