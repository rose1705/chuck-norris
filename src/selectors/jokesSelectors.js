export const isLoading = ({ jokesReducer }) =>
    jokesReducer && jokesReducer.isLoading;

export const getJokes = ({ jokesReducer }) =>
    jokesReducer && jokesReducer.jokes;

export const getFavorites = ({ favoritesReducer }) =>
    favoritesReducer && favoritesReducer.favorites;
