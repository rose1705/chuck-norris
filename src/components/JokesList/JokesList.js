import React from "react";
import "../../styles/JokesList.scss";

const JokesList = ({
    fetchTenJokes,
    addToFavorites,
    favorites,
    jokes,
    formatJokeText,
}) => {
    let idArray = favorites.map(f => f.id);
    return (
        <div className="JokesList">
            <div className="JokesList__header">
                <span>List of Jokes</span>
                <button onClick={fetchTenJokes}>Time to Laugh</button>
            </div>
            <ul className="JokesList__list">
                {jokes.map(joke => (
                    <li key={joke.id} className="JokesList__list__item">
                        <button
                            className={
                                idArray.includes(joke.id) ? "selected" : ""
                            }
                            onClick={() => addToFavorites(joke)}
                            disabled={favorites.length >= 10}
                        >
                            {idArray.includes(joke.id)
                                ? `Already Added`
                                : `Add to Favorites`}
                        </button>
                        <p
                            dangerouslySetInnerHTML={formatJokeText(joke.joke)}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default JokesList;
