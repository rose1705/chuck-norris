import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import FavoritesList from "./FavoritesList";

import {
    removeFromFavorites,
    addRandomToFavorite,
} from "../../actions/jokesActions";
import { getFavorites } from "../../selectors/jokesSelectors";

const mapStateToProps = state => ({
    favorites: getFavorites(state),
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            removeFromFavorites,
            addRandomToFavorite,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(FavoritesList);
