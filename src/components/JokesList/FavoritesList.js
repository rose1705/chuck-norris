import React from "react";
import "../../styles/JokesList.scss";

//TODO: clean up code
//TODO: change button to toggle
//TODO: fix adding two of the same jokes when adding random jokes
//TODO: add notification if Favorites list is full

class FavoritesList extends React.Component {
    state = {
        isTimerOn: false,
    };

    addRandomJokeToFavorites = () => {
        const { favorites, addRandomToFavorite } = this.props;
        if (this.state.isTimerOn && favorites.length < 10) {
            //Add random joke as long as list is 10 items or less
            addRandomToFavorite();
        } else {
            this.stopAddingJokes();
        }
    };

    stopAddingJokes = () => {
        //Clear interval ref
        clearInterval(this.interval);
        this.setState({ isTimerOn: false });
    };

    toggleTimer = () => {
        if (this.state.isTimerOn) {
            this.stopAddingJokes();
        } else {
            this.setState({ isTimerOn: true });
            this.interval = setInterval(() => {
                this.addRandomJokeToFavorites();
            }, 5000);
        }
    };

    render() {
        const { formatJokeText, removeFromFavorites, favorites } = this.props;
        const { isTimerOn } = this.state;
        return (
            <div className="FavoritesList">
                <div className="FavoritesList__header">
                    <span>Favorites</span>
                    <button
                        onClick={this.toggleTimer}
                        className={isTimerOn ? "active" : ""}
                    >
                        {`Turn Random Favorite ${isTimerOn ? "OFF" : "ON"}`}
                    </button>
                </div>
                <ul className="FavoritesList__list">
                    {favorites.map(favorite => (
                        <li
                            key={favorite.id}
                            className="FavoritesList__list__item"
                        >
                            <button
                                onClick={() => removeFromFavorites(favorite)}
                            >
                                Remove from Favorites
                            </button>
                            <p
                                dangerouslySetInnerHTML={formatJokeText(
                                    favorite.joke,
                                )}
                            />
                            >
                        </li>
                    ))}
                </ul>
            </div>
        );
    }
}

export default FavoritesList;
