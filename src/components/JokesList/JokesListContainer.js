import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import JokesList from "./JokesList";

import { fetchTenJokes, addToFavorites } from "../../actions/jokesActions";
import {
    isLoading,
    getJokes,
    getFavorites,
} from "../../selectors/jokesSelectors";

const mapStateToProps = state => ({
    isLoading: isLoading(state),
    jokes: getJokes(state),
    favorites: getFavorites(state),
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
            fetchTenJokes,
            addToFavorites,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(JokesList);
