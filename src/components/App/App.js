import React, { useState } from "react";
import "../../styles/App.scss";
import JokesListContainer from "../JokesList/JokesListContainer";
import FavoritesListContainer from "../JokesList/FavoritesListContainer";

function App() {
    const [listTab, toggleTabs] = useState(false);

    const formatJokeText = joke => ({ __html: joke });

    return (
        <div className="App">
            <h1>Chuck Norris Joke Machine</h1>
            <div className="App__tabs">
                <div
                    className={
                        listTab ? `App__tabs__tab notActive` : `App__tabs__tab`
                    }
                    onClick={() => toggleTabs(false)}
                >
                    Random
                </div>
                <div
                    className={
                        listTab ? `App__tabs__tab` : `App__tabs__tab notActive`
                    }
                    onClick={() => toggleTabs(true)}
                >
                    Favorites
                </div>
            </div>

            {listTab ? (
                <FavoritesListContainer formatJokeText={formatJokeText} />
            ) : (
                <JokesListContainer formatJokeText={formatJokeText} />
            )}
        </div>
    );
}

export default App;
