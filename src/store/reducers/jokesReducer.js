import constants from "../../config/constants";

const initialState = {
    isLoading: false,
    jokes: [],
};

const jokesReducer = (state = initialState, { type, data }) => {
    switch (type) {
        case constants.FETCHING_JOKES:
            return {
                ...state,
                isLoading: true,
            };
        case constants.JOKES_RECEIVED:
            return {
                ...state,
                isLoading: false,
                jokes: data.value,
            };

        default:
            return state;
    }
};

export default jokesReducer;
