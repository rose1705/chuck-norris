import { combineReducers } from "redux";
import jokesReducer from "./jokesReducer";
import favoritesReducer from "./favoritesReducer";

export default combineReducers({
    jokesReducer,
    favoritesReducer,
});
