import constants from "../../config/constants";

const initialState = {
    favorites: [],
};

const favoritesReducer = (state = initialState, { type, data }) => {
    switch (type) {
        case constants.ADD_FAVORITE:
            return {
                ...state,
                favorites: Array.from(new Set([...state.favorites, data])),
            };
        case constants.REMOVE_FAVORITE:
            return {
                ...state,
                favorites: state.favorites.filter(
                    favorite => favorite.id !== data.id,
                ),
            };

        default:
            return state;
    }
};

export default favoritesReducer;
