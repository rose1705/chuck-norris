import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers/index";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunkMiddleware from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

const persistConfig = {
    key: "root",
    storage,
    whitelist: ["favoritesReducer"],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
    let store =
        process.env.NODE_ENV === "production"
            ? createStore(persistedReducer, applyMiddleware(thunkMiddleware))
            : createStore(
                  persistedReducer,
                  composeWithDevTools(applyMiddleware(thunkMiddleware)),
              );
    let persistor = persistStore(store);

    return { store, persistor };
};
